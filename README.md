# Projet AI28 - Machine Learning
## Membres
RAMOS IRETA José Santiago
LUDOVIC Turpin

## Description
Analyse de dataset 'Bank Marketing' et prediction d'une variable target avec les methodologies vues en cours tels que les regressions penalisées, SVM, et arbres descissionels

## Rapport
https://www.overleaf.com/8269413675gtjssbkmmsvq

## Installation
##### Pour télécharger:
Tapez les commandes successives dans le repertoire de votre choix:
1. SSH
```bash
git clone git@gitlab.utc.fr:jramosir/ai28_p22.git
```
2. HTTP
```bash
git clone https://gitlab.utc.fr/jramosir/ai28_p22.git
```
```bash
cd ai28_p22											#on se place dans le répertoire git
```
##### Pour télécharger les mises à jour (il faut le faire avant toute modification)
```bash 
git pull
```
##### Pour mettre à jour ses modifications
```bash
git commit -a -m "message associé au commit"        # commit = sauvegarder en local (l'option -a veut dire all)
git push 											# pour uploader tous le code modifié (commited)
```

##### Pour ajouter tous les nouveaux fichiers créés localement au projet
```bash
git add * 										    # à faire avant le nouveau commit
```
